package password;

import javax.swing.JOptionPane;

public class passwordTest {

	public static void main(String[] args) 
	{
		try
		{
			
		String pass = JOptionPane.showInputDialog(null,
			"Enter a password", "Enter",
			JOptionPane.QUESTION_MESSAGE);
		
		String x = validatePass(pass);
		JOptionPane.showMessageDialog(null, x, "Password",
			JOptionPane.INFORMATION_MESSAGE);
		
				}
		catch(StringIndexOutOfBoundsException e)
		{
			JOptionPane.showMessageDialog(null,
					"You entered the time in the wrong format.\n" +
					"Please enter the time in the form hh:mm:ss",
					"Invalid Time", JOptionPane.ERROR_MESSAGE);
		}
		catch(NumberFormatException e)
		{
			JOptionPane.showMessageDialog(null,
					"You entered an invalid time.\nPlease enter numbers only.",
					"Invalid Time",	JOptionPane.ERROR_MESSAGE);
		}
		catch(Exception e)
		{
			System.out.println("An unexpected Exception occurred");
		}
	}
	

	public static String validatePass(String pass)throws NumberFormatException, StringIndexOutOfBoundsException {
		{
		int x = pass.length();
		int count =0;
		
		for (int i = 0; i < x; i++)
		{
			   char z = pass.charAt(i);
	            if (isNum(z))
	            {
	            	count++;
	            }
		}
		if (count < 2 && x > 8)
		{
			return("Password lacking 2 digits");
		}
		else if (x < 8)
		{
			return ("Password too short");
		}
		else 
		{
			return ("Password saved");
		}
		}
	}
	//created boolean test for char by comparing char rather then int
    public static boolean isNum(char x) {

        return (x >= '0' && x<= '9');
    }


	
}


